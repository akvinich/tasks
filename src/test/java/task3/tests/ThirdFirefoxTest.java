package task3.tests;


import base.BaseFirefoxTest;
import org.testng.Assert;
import org.testng.annotations.*;
import task3.pages.*;

public class ThirdFirefoxTest extends BaseFirefoxTest{

    private PassengerInfoPage passengerInfoPage = null;
    private TripSummaryPage tripSummaryPage = null;
    private DeltaMainPage deltaMainPage = null;
    private DeltaSelectPage deltaSelectPage = null;
    private CreditCardInfoPage creditCardInfoPage = null;

    public void runner(){

    }

    @DataProvider
    public Object[][] deltaMainPageData(){
        return new Object[][]{
                {"FLIGHT", "ROUND TRIP", "JFK", "SVO", "05/28/2018", "05/31/2018", "EXACT DATES", "MONEY"},
        };
    }

    @DataProvider
    public Object[][] deltaSelectPageData(){
        return new Object[][]{
                {"MAIN CABIN"},
        };
    }

    @DataProvider
    public Object[][] passengerInfo(){
        return new Object[][]{
                {"Igor", "Marinin", "Male", "April", "29", "1981", "Igor", "Marinin", "Belarus (375)", "298787666", "Home", "Belarus (375)", "298787666", "igormarinin7@gmail.com", "igormarinin7@gmail.com"},
        };
    }


    @Test(dataProvider = "deltaMainPageData")
    public void deltaMainPageTest(String kindOfTripStr, String wayOfTripStr, String fromStr, String toStr, String departDateStr, String returnDateStr, String travelDateStr, String showPriceInStr){
        deltaMainPage = new DeltaMainPage(firefox);
        deltaSelectPage =
                deltaMainPage.open().selectInMainMenuBookATrip(kindOfTripStr, wayOfTripStr, fromStr, toStr, departDateStr, returnDateStr, travelDateStr, showPriceInStr);
    }

    @Test(dependsOnMethods = {"deltaMainPageTest"}, dataProvider = "deltaSelectPageData")
    public void deltaSelectPageTest(String comfort){
        tripSummaryPage = deltaSelectPage.selectDepartureAndReturnTickets(comfort);
    }

    @Test(dependsOnMethods = {"deltaSelectPageTest"})
    public void tripSummaryPageTest(){
        passengerInfoPage = tripSummaryPage.clickContinue();
    }

    @Test(dependsOnMethods = {"tripSummaryPageTest"}, dataProvider = "passengerInfo")
    public void passengerInfoPageTest(String passengerFirstName,
                                  String passengerLastName,
                                  String passengerGender,
                                  String passengerDateOfBirthMonth,
                                  String passengerDateOfBirthDay,
                                  String passengerDateOfBirthYear,
                                  String contactFirstName,
                                  String contactLastName,
                                  String contactCountryRegion,
                                  String contactPhoneNumber,
                                  String contactDeviceType,
                                  String contactCountryCoded,
                                  String contactTelephoneNumber,
                                  String contactEmail,
                                  String contactConfirmEmail){

        creditCardInfoPage =
        passengerInfoPage.fillAllRequiredPassengerInformationFields(passengerFirstName,
                                                                    passengerLastName,
                                                                    passengerGender,
                                                                    passengerDateOfBirthMonth,
                                                                    passengerDateOfBirthDay,
                                                                    passengerDateOfBirthYear,
                                                                    contactFirstName,
                                                                    contactLastName,
                                                                    contactCountryRegion,
                                                                    contactPhoneNumber,
                                                                    contactDeviceType,
                                                                    contactCountryCoded,
                                                                    contactTelephoneNumber,
                                                                    contactEmail,
                                                                    contactConfirmEmail);

    }

    @Test(dependsOnMethods = {"passengerInfoPageTest"})
    public void creditCardInfoPageTest(){
        Assert.assertEquals(creditCardInfoPage.checkButtonCompletePurchaseIsActive(), true);
    }

}
