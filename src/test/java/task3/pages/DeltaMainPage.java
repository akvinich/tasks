package task3.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.AbstractPage;

public class DeltaMainPage extends AbstractPage {


    public DeltaMainPage(WebDriver driver) {
        super(driver);
    }

    public DeltaMainPage open() {
        this.open("https://www.delta.com/");
        return new DeltaMainPage(driver);
    }

    public void selectInMainMenuMyTrips() {
    }

    public void selectInMainFlightStatus() {
    }

    public void selectInMainCheckIn() {
    }

    public DeltaSelectPage selectInMainMenuBookATrip(String kindOfTripStr, String wayOfTripStr, String fromStr, String toStr, String departDateStr, String returnDateStr, String travelDateStr, String showPriceInStr) {
        WebElement kindOfTrip = null;
        WebElement wayOfTrip = null;
        WebElement from = null;
        WebElement to = null;
        WebElement departDate = null;
        WebElement returnDate = null;
        WebElement travelDate = null;
        WebElement showPriceIn = null;
        WebElement findFlights = null;

        switch (kindOfTripStr.toUpperCase()) {
            case "FLIGHT":
                kindOfTrip = findById("book-air-content-trigger");
                break;
            case "HOTEL":
                break;
            case "CAR":
                break;
            case "VACATION PACKAGES":
                break;
            default:
                throw new RuntimeException();
        }

        switch (wayOfTripStr.toUpperCase()) {
            case "ROUND TRIP":
                wayOfTrip = findById("roundTripBtn");
                break;
            case "ONE WAY":
                break;
            case "MULTI-CITY":
                break;
            default:
                throw new RuntimeException();
        }

        from = findById("originCity");
        to = findById("destinationCity");

        departDate = findById("departureDate");
        returnDate = findById("returnDate");

        switch (travelDateStr.toUpperCase()) {
            case "EXACT DATES":
                travelDate = findById("exactDaysBtn");
                break;
            case "FLEXIBLE DAYS":
                break;
            default:
                throw new RuntimeException();
        }

        switch (showPriceInStr.toUpperCase()) {
            case "MONEY":
                showPriceIn = findById("cashBtn");
                break;
            case "MILES":
                break;
            default:
                throw new RuntimeException();
        }

        findFlights = findById("findFlightsSubmit");

        kindOfTrip.click();
        wayOfTrip.click();
        from.clear();
        from.sendKeys(fromStr.toUpperCase());
        to.clear();
        to.sendKeys(toStr.toUpperCase());
        departDate.sendKeys(departDateStr);
        returnDate.sendKeys(returnDateStr);
        travelDate.click();
        showPriceIn.click();
        findFlights.click();

        return new DeltaSelectPage(driver);
    }


    //=======================================details==============================

}