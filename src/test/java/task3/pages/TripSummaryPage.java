package task3.pages;


import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.AbstractPage;

public class TripSummaryPage extends AbstractPage{

    private String buttonContinueIdLocator = "tripSummarySubmitBtn";

    public TripSummaryPage(WebDriver driver) {
        super(driver);
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlContains("tripsummary"));
    }

    public PassengerInfoPage clickContinue(){
        this.waitVisibilityOfElement(this.findById(buttonContinueIdLocator));
        ((JavascriptExecutor)driver).executeScript("return jQuery.active == 0");
        this.findById(buttonContinueIdLocator).click();
        return new PassengerInfoPage(driver);
    }
}
