package task3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.AbstractPage;

import java.util.List;

public class DeltaSelectPage extends AbstractPage{

    By listTicketsLocator = By.cssSelector("table.flightResultTable");
    By mainCabinComfortLocator = By.cssSelector("td.brandWrapperMAIN");
    By deltaComfortLocator = By.cssSelector("td.brandWrapperDCP");
    By premiumSelectComfortLocator = By.cssSelector("td.brandWrapperDPPS");
    By deltaOneComfortLocator = By.cssSelector("td.brandWrapperD1S");

    String tripLocationCssLocator = "div.tripLocation";


    public DeltaSelectPage(WebDriver driver) {
        super(driver);
    }

    public TripSummaryPage selectDepartureAndReturnTickets(String comfort){
        List<WebElement> listTickets = driver.findElements(listTicketsLocator);
        this.selectTicket(listTickets, comfort);
        this.findByCss(tripLocationCssLocator);
        listTickets = driver.findElements(listTicketsLocator);
        this.selectTicket(listTickets, comfort);
        return new  TripSummaryPage(driver);
    }

//=========================details==============================
    private void selectTicket(List<WebElement> listTickets, String comfort){
        System.out.println(listTickets.size());
        switch (comfort.toUpperCase()) {
            case "MAIN CABIN":
                listTickets.get(0).findElement(mainCabinComfortLocator).findElement(By.tagName("a")).click();
                break;
            case "DELTA COMFORT":
                listTickets.get(0).findElement(deltaComfortLocator).findElement(By.tagName("a")).click();
                break;
            case "PREMIUM SELECT":
                listTickets.get(0).findElement(premiumSelectComfortLocator).findElement(By.tagName("a")).click();
                break;
            case "DELTA ONE":
                listTickets.get(0).findElement(deltaOneComfortLocator).findElement(By.tagName("a")).click();
                break;
            default:
                throw new RuntimeException();
        }
    }


}
