package task3.pages;

import org.openqa.selenium.WebDriver;
import pages.AbstractPage;

public class CreditCardInfoPage extends AbstractPage{

    private String completePurchaseIdLocator = "continue_button";

    public CreditCardInfoPage(WebDriver driver) {
        super(driver);
    }

    public boolean checkButtonCompletePurchaseIsActive(){
        return this.findById(completePurchaseIdLocator).isEnabled();
    }

}
