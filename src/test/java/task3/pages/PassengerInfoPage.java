package task3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import pages.AbstractPage;

import java.util.List;

public class PassengerInfoPage extends AbstractPage{

// passenger
    private String passengerPrefixIdLocator = "prefix0";
    private String passengerFirstNameIdLocator = "firstName0"; //required
    private String passengerMiddleNameIdLocator = "middlename0";
    private String passengerLastNameIdLocator = "lastName0"; //required
    private String passengerSuffixIdLocator = "suffix0";
    private String passengerFrequentFlyerProgramIdLocator = "airlineprogram0";
    private String passengerFrequentFlyerNumberIdLocator = "frequentFlyerNumber0";
    private String passengerGenderIdLocator = "gender0"; //required
    private String passengerDateOfBirthMonthIdLocator = "month0"; //required
    private String passengerDateOfBirthDayIdLocator = "day0"; //required
    private String passengerDateOfBirthYearIdLocator = "year0"; //required
    private String passengerKnownTravelerNumberIdLocator = "travelernbr0";
    private String passengerRedressNumberIdLocator = "redressnbr0";
// Emergency Contact Information
    private String contactFirstNameIdLocator = "emgcFirstName_0"; //required
    private String contactLastNameIdLocator = "emgcLastName_0"; //required
    private String contactCountryRegionIdLocator = "emgcCountry_0"; //required
    private String contactPhoneNumberIdLocator = "emgcPhoneNumber_0"; //required
// Contact Information
    private String contactDeviceTypeIdLocator = "deviceType"; //required
    private String contactCountryCodedIdLocator = "countryCode0"; //required
    private String contactTelephoneNumberIdLocator = "telephoneNumber0"; //required
    private String contactEmailIdLocator = "email"; //required
    private String contactConfirmEmailIdLocator = "reEmail"; //required
//
    private String continueButtonIdLocator = "paxReviewPurchaseBtn";

    private By spanValueLocator = By.cssSelector("span.ui-selectmenu-text");

    private String genderSelectContainerIdLocator = "gender0-button";
    private String monthSelectContainerIdLocator = "month0-button";
    private String daySelectContainerIdLocator = "day0-button";
    private String yearSelectContainerIdLocator = "year0-button";
    private String emgcCountrySelectContainerIdLocator = "emgcCountry_0-button";
    private String deviceTypeSelectContainerIdLocator = "deviceType-button";
    private String countrySelectContainerIdLocator = "countryCode0-button";


    public PassengerInfoPage(WebDriver driver) {
        super(driver);
    }

    public CreditCardInfoPage fillAllRequiredPassengerInformationFields(String passengerFirstName,
                                                                        String passengerLastName,
                                                                        String passengerGender,
                                                                        String passengerDateOfBirthMonth,
                                                                        String passengerDateOfBirthDay,
                                                                        String passengerDateOfBirthYear,
                                                                        String contactFirstName,
                                                                        String contactLastName,
                                                                        String contactCountryRegion,
                                                                        String contactPhoneNumber,
                                                                        String contactDeviceType,
                                                                        String contactCountryCoded,
                                                                        String contactTelephoneNumber,
                                                                        String contactEmail,
                                                                        String contactConfirmEmail){

        this.findById(passengerFirstNameIdLocator).sendKeys(passengerFirstName);
        this.findById(passengerLastNameIdLocator).sendKeys(passengerLastName);
        this.selectBoxInsertOption(passengerGender, genderSelectContainerIdLocator, passengerGenderIdLocator);
        this.selectBoxInsertOption(passengerDateOfBirthMonth, monthSelectContainerIdLocator, passengerDateOfBirthMonthIdLocator);
        this.selectBoxInsertOption(passengerDateOfBirthDay, daySelectContainerIdLocator, passengerDateOfBirthDayIdLocator);
        this.selectBoxInsertOption(passengerDateOfBirthYear, yearSelectContainerIdLocator, passengerDateOfBirthYearIdLocator);
        this.findById(contactFirstNameIdLocator).sendKeys(contactFirstName);
        this.findById(contactLastNameIdLocator).sendKeys(contactLastName);
        this.selectBoxInsertOption(contactCountryRegion, emgcCountrySelectContainerIdLocator, contactCountryRegionIdLocator);
        this.findById(contactPhoneNumberIdLocator).sendKeys(contactPhoneNumber);
        this.selectBoxInsertOption(contactDeviceType, deviceTypeSelectContainerIdLocator, contactDeviceTypeIdLocator);
        this.selectBoxInsertOption(contactCountryCoded, countrySelectContainerIdLocator, contactCountryCodedIdLocator);
        this.findById(contactTelephoneNumberIdLocator).sendKeys(contactTelephoneNumber);
        this.findById(contactEmailIdLocator).sendKeys(contactEmail);
        this.findById(contactConfirmEmailIdLocator).sendKeys(contactConfirmEmail);

        this.findById(continueButtonIdLocator).click();

        return new CreditCardInfoPage(driver);
    }

//========================details=========================================================

    private void selectBoxInsertOption(String option, String selectContainerId, String selectId){
        WebElement passengerGenderContainer = this.findById(selectContainerId).findElement(spanValueLocator);
        this.scrollIntoViewWithJavascriptExecutor(option, this.findById(selectId));
        passengerGenderContainer.click();
        passengerGenderContainer.click();
    }


}
