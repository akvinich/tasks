

import task5.Runner;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class RunnerAll {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        // src-->test-->java-->Run all test



//===============================================================================================================
        task4.Runner runner4 = new task4.Runner();
        Class reflectRunner4 = runner4.getClass();
        Class[] paramTypes4 = new  Class[]{String[].class};

        Method method4 = reflectRunner4.getDeclaredMethod("main", paramTypes4);
        Object[] ar4 = new Object[]{new String[]{""}};
        method4.invoke(runner4,ar4);

//===============================================================================================================
        Runner runner = new Runner();
        Class reflectRunner = runner.getClass();
        Class[] paramTypes = new  Class[]{String[].class};

        Method method5 = reflectRunner.getDeclaredMethod("main", paramTypes);
        Object[] ar = new Object[]{new String[]{""}};
        method5.invoke(runner,ar);



    }

}
