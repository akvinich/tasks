package task4;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleFTP {

    final static Logger rootLogger = LogManager.getRootLogger();
    /**
     * Create an instance of SimpleFTP.
     */
    public SimpleFTP() {

    }

    /**
     * Connects to the default port of an FTP server and logs in as
     * anonymous/anonymous.
     */
    public synchronized void connect(String host) throws IOException {
        connect(host, 21);
    }

    /**
     * Connects to an FTP server and logs in as anonymous/anonymous.
     */
    public synchronized void connect(String host, int port) throws IOException {
        connect(host, port, "anonymous", "anonymous");
    }

    /**
     * Connects to an FTP server and logs in with the supplied username and
     * password.
     */
    public synchronized void connect(String host, int port, String user,
                                     String pass) throws IOException {
        if (socket != null) {
            throw new IOException("SimpleFTP is already connected. Disconnect first.");
        }
        socket = new Socket(host, port);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(
                new OutputStreamWriter(socket.getOutputStream()));

        String response = readLine();
        if (!response.startsWith("220 ")) {
            throw new IOException(
                    "SimpleFTP received an unknown response when connecting to the FTP server: "
                            + response);
        }

        sendLine("USER " + user);

        response = readLine();
        if (!response.startsWith("331 ")) {
            throw new IOException(
                    "SimpleFTP received an unknown response after sending the user: "
                            + response);
        }

        sendLine("PASS " + pass);

        response = readLine();
        if (!response.startsWith("230 ")) {
            throw new IOException(
                    "SimpleFTP was unable to log in with the supplied password: "
                            + response);
        }

        rootLogger.info("go to "+ host);
        // Now logged in.
    }

    /**
     * Disconnects from the FTP server.
     */
    public synchronized void disconnect() throws IOException {
        try {
            sendLine("QUIT");
        } finally {
            socket = null;
        }
        rootLogger.info("disconnect!");
    }

    /**
     * Returns the working directory of the FTP server it is connected to.
     */
    public synchronized String pwd() throws IOException {
        sendLine("PWD");
        String dir = null;
        String response = readLine();
        if (response.startsWith("257 ")) {
            int firstQuote = response.indexOf('\"');
            int secondQuote = response.indexOf('\"', firstQuote + 1);
            if (secondQuote > 0) {
                dir = response.substring(firstQuote + 1, secondQuote);
            }
        }
        return dir;
    }

    /**
     * Changes the working directory (like cd). Returns true if successful.
     */
    public synchronized boolean cwd(String dir) throws IOException {
        sendLine("CWD " + dir);
        String response = readLine();
        return (response.startsWith("250 "));
    }

    public synchronized boolean cdup() throws IOException {
        sendLine("CDUP");
        String response = readLine();
        return (response.startsWith("250 "));
    }

    public synchronized boolean mkb(String dir) throws IOException {
        sendLine("MKD " + dir);
        String response = readLine();
        return (response.startsWith("257 "));
    }

    public synchronized boolean dele(String file) throws IOException {
        sendLine("DELE " + file);
        String response = readLine();
        return (response.startsWith("250 "));
    }

    /**
     * Sends a file to be stored on the FTP server. Returns true if the file
     * transfer was successful. The file is sent in passive mode to avoid NAT or
     * firewall problems at the client end.
     */
    public synchronized boolean stor(File file) throws IOException {
        if (file.isDirectory()) {
            throw new IOException("SimpleFTP cannot upload a directory.");
        }

        String filename = file.getName();

        return stor(new FileInputStream(file), filename);
    }

    /**
     * Sends a file to be stored on the FTP server. Returns true if the file
     * transfer was successful. The file is sent in passive mode to avoid NAT or
     * firewall problems at the client end.
     */
    public synchronized boolean stor(InputStream inputStream, String filename)
            throws IOException {

        BufferedInputStream input = new BufferedInputStream(inputStream);

        sendLine("PASV");
        String response = readLine();
        if (!response.startsWith("227 ")) {
            throw new IOException("SimpleFTP could not request passive mode: "
                    + response);
        }

        String ip = null;
        int port = -1;
        int opening = response.indexOf('(');
        int closing = response.indexOf(')', opening + 1);
        if (closing > 0) {
            String dataLink = response.substring(opening + 1, closing);
            StringTokenizer tokenizer = new StringTokenizer(dataLink, ",");
            try {
                ip = tokenizer.nextToken() + "." + tokenizer.nextToken() + "."
                        + tokenizer.nextToken() + "." + tokenizer.nextToken();
                port = Integer.parseInt(tokenizer.nextToken()) * 256
                        + Integer.parseInt(tokenizer.nextToken());
            } catch (Exception e) {
                throw new IOException("SimpleFTP received bad data link information: "
                        + response);
            }
        }

        sendLine("STOR " + filename);

        Socket dataSocket = new Socket(ip, port);

        response = readLine();
        if (!response.startsWith ("125 ")) {
            //if (!response.startsWith("150 ")) {
            throw new IOException("SimpleFTP was not allowed to send the file: "
                    + response);
        }

        BufferedOutputStream output = new BufferedOutputStream(dataSocket
                .getOutputStream());
        byte[] buffer = new byte[4096];
        int bytesRead = 0;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
        output.flush();
        output.close();
        input.close();

        response = readLine();
        return response.startsWith("226 ");
    }

    /**
     * Enter binary mode for sending binary files.
     */
    public synchronized boolean bin() throws IOException {
        sendLine("TYPE I");
        String response = readLine();
        return (response.startsWith("200 "));
    }

    /**
     * Enter ASCII mode for sending text files. This is usually the default mode.
     * Make sure you use binary mode if you are sending images or other binary
     * data, as ASCII mode is likely to corrupt them.
     */
    public synchronized boolean ascii() throws IOException {
        sendLine("TYPE A");
        String response = readLine();
        return (response.startsWith("200 "));
    }

    /**
     * Sends a raw command to the FTP server.
     */
    public void sendLine(String line) throws IOException {
        if (socket == null) {
            throw new IOException("SimpleFTP is not connected.");
        }
        try {
            writer.write(line + "\r\n");
            writer.flush();
            if (DEBUG) {
                System.out.println("> " + line);
            }
        } catch (IOException e) {
            socket = null;
            throw e;
        }
    }

    public String readLine() throws IOException {
        String line = reader.readLine();
        if (DEBUG) {
            System.out.println("< " + line);
        }
        return line;
    }

    private Socket socket = null;

    private BufferedReader reader = null;

    private BufferedWriter writer = null;

    private static boolean DEBUG = false;


    public synchronized List<String> getList() throws IOException {
        String[] getListCommands = new String[]{"LIST", "NLST"};
        List<String> aList = null;
        List<String> aListNew = null;
        for (int m=0; m<getListCommands.length; m++) {
            sendLine("PASV");
            String response = readLine();
            if (!response.startsWith("227 ")) {
                throw new IOException("SimpleFTP could not request passive mode: "
                        + response);
            }

            String ip = null;
            int port = -1;
            int opening = response.indexOf('(');
            int closing = response.indexOf(')', opening + 1);
            if (closing > 0) {
                String dataLink = response.substring(opening + 1, closing);
                StringTokenizer tokenizer = new StringTokenizer(dataLink, ",");
                try {
                    ip = tokenizer.nextToken() + "." + tokenizer.nextToken() + "."
                            + tokenizer.nextToken() + "." + tokenizer.nextToken();
                    port = Integer.parseInt(tokenizer.nextToken()) * 256
                            + Integer.parseInt(tokenizer.nextToken());
                } catch (Exception e) {
                    throw new IOException("SimpleFTP received bad data link information: "
                            + response);
                }
            }


            switch (getListCommands[m]){
                case "LIST":
                    sendLine("LIST");
                    Socket dataSocket = new Socket(ip, port);
                    StringBuffer buffer = new StringBuffer();
                    try (BufferedInputStream bis = new BufferedInputStream(dataSocket.getInputStream())) {
                        int c;
                        while ((c = bis.read()) != -1) {
                            buffer.append((char) c);
                        }

                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    aList = new ArrayList(Arrays.asList(buffer.toString().split("\n")));
                    readLine();
                    readLine();
                    break;

                case "NLST":
                    sendLine("NLST");
                    Socket dataSocketNew = new Socket(ip, port);

                    StringBuffer bufferNew = new StringBuffer();
                    try (BufferedInputStream bis = new BufferedInputStream(dataSocketNew.getInputStream())) {
                        int c;
                        while ((c = bis.read()) != -1) {
                            bufferNew.append((char) c);
                        }

                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    aListNew = new ArrayList(Arrays.asList(bufferNew.toString().split("\n")));
                    readLine();
                    readLine();
                    break;

            }

        }
        int size = aList.size();
        for (int i = 0; i < size; i++) {
            if(!containsIgnoreCase(aList.get(i), "drwxr-xr-x")){
                aListNew.remove(i);
                aList.remove(i);
                i--;
                size--;
            }
        }
        return aListNew;
    }

    public boolean isDirectory(String str){
        return  true;
    }


    //=========================details=================================================
    private boolean containsIgnoreCase(final String haystack, final String needle){
        if(needle.equals(""))
            return true;
        if(haystack == null || needle == null || haystack .equals(""))
            return false;

        Pattern p = Pattern.compile(needle,Pattern.CASE_INSENSITIVE+Pattern.LITERAL);
        Matcher m = p.matcher(haystack);
        return m.find();
    }
}