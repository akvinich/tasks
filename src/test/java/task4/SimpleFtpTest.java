package task4;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

public class SimpleFtpTest {
    final static Logger rootLogger = LogManager.getRootLogger();
    @Test
    public void ftpTest() throws IOException {

        SimpleFTP simpleFTP = new SimpleFTP();

        simpleFTP.connect("ftp.martin.dk");


        List<String> directories = simpleFTP.getList();

        for (int i=0; i<directories.size(); i++){
            simpleFTP.cwd(directories.get(i));
            rootLogger.info("go to " + simpleFTP.pwd());
            simpleFTP.cdup();
            rootLogger.info("back");
        }
        rootLogger.info(simpleFTP.pwd());

        String newDir="newDir";
        boolean isCreateDir = simpleFTP.mkb(newDir);
        rootLogger.info("New directory '"+ newDir + "' created: "+ isCreateDir);
        if(isCreateDir){
            simpleFTP.dele(newDir);
        }

        simpleFTP.disconnect();

    }
}
