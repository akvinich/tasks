package task2.tests;

import base.BaseFirefoxTest;
import org.apache.log4j.*;
import org.testng.annotations.*;
import task2.pages.*;

public class SecondFirefoxTest extends BaseFirefoxTest{

    final static Logger rootLogger = LogManager.getRootLogger();

    @DataProvider
    public Object[][] task2(){
        return new Object[][]{
                {"igormarinin7@gmail.com", "tellurium500", "google", "vital1234@tut.by", "mail", "new mail"},
        };
    }

    @Test(dataProvider = "task2")
    public void logInAndSendMail(String login, String pass, String searchMessage, String emailReceiver, String topicEmail, String bodyMail){
        AccountPage accountPage =
            new StartMailPage(firefox).open().clickEnterEmail().
                logIn(login,pass).openOutgoingMessages()
                .openSpamMessages().openIncomingMessages();

        rootLogger.info("fouund " + accountPage.getQuantityResultsOfSearch(searchMessage)+ " messages");

        rootLogger.info(accountPage.getListSearchResults().toString());

        accountPage = accountPage.writeEmail(emailReceiver, topicEmail, bodyMail);

        accountPage.logOut();

    }


}
