package task2.pages;

import org.openqa.selenium.WebDriver;
import pages.AbstractPage;

public class StartMailPage extends AbstractPage{

    private final String linkEnterEmailCssLocator = "a.gmail-nav__nav-link.gmail-nav__nav-link__sign-in";

    public StartMailPage(WebDriver driver) {
        super(driver);
    }

    public StartMailPage open() {
        this.open("https://www.google.com/intl/ru/gmail/about/#");
        return new StartMailPage(driver);
    }

    public LoginPage clickEnterEmail(){
        this.findByCss(linkEnterEmailCssLocator).click();
        return new LoginPage(driver);
    }
}
