package task2.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.AbstractPage;

public class LoginPage extends AbstractPage {

    private final String inputLoginIdLocator = "identifierId";
    private final String inputPasswordCssLocator = "#password > div.aCsJod.oJeWuf > div > div.Xb9hP > input";

    private final String buttonLoginNextIdLocator = "identifierNext";
    private final String buttonPasswordNextIdLocator  = "passwordNext";

    public LoginPage(WebDriver driver) {
        super(driver);
    }


    public AccountPage logIn(String log, String pass) {

        this.findById(inputLoginIdLocator).sendKeys(log);
        this.findById(buttonLoginNextIdLocator ).click();
        this.waitVisibilityOfElement(this.findByCss(inputPasswordCssLocator));
        this.findByCss(inputPasswordCssLocator).sendKeys(pass);
        this.findById(buttonPasswordNextIdLocator ).click();

        return new AccountPage(driver);
    }


}
