package task2.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.AbstractPage;

import java.util.ArrayList;
import java.util.List;

public class AccountPage extends AbstractPage{

    private String fieldSearchIdLocator = "gbqfq";

    private String buttonSearchIdLocator = "gbqfb";

    private String buttonWriteClassNameLocator = "T-I-KE";

    private String incomingMessagesCssLocator = "a.J-Ke[title=Входящие]";

    private String outgoingMessagesCssLocator = "a.J-Ke[title=Отправленные]";

    private String elseCollapseIdLocator = "span.CJ";

    private String spamCssLocator = "a.J-Ke[title=Спам]";

    private By emailCssLocator = By.cssSelector("tr.zA.yO");

    //LogOut Elements
    private String logOutIconCssLocator = "div.gb_Sc.gb_mb.gb_Pg.gb_R";
    private String logOutButtonIdLocator = "gb_71";

    //Write email Elements
    private String emailInputCssLocator = "textarea[aria-label=Кому]";
    private String topicInputCssLocator = "input[name=subjectbox";
    private String bodyMailCssLocator = "div[aria-label='Тело письма'";
    private String buttonSendMailCssLocator = "div.T-I.J-J5-Ji.aoO.T-I-atl.L3";



    public AccountPage(WebDriver driver) {
        super(driver);
    }

    public AccountPage openIncomingMessages(){
        this.findByCss(incomingMessagesCssLocator).click();
        if(!this.isNeedleDirectory(Directories.inbox.toString())){throw new RuntimeException();}
        return this;
    }

    public AccountPage openOutgoingMessages(){
        this.findByCss(outgoingMessagesCssLocator).click();
        if(!this.isNeedleDirectory(Directories.sent.toString())){throw new RuntimeException();}
        return this;
    }

    public AccountPage openSpamMessages(){
        this.findByCss(elseCollapseIdLocator).click();
        this.findByCss(spamCssLocator).click();
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(!this.isNeedleDirectory(Directories.spam.toString())){throw new RuntimeException();}
        return this;
    }

    public int getQuantityResultsOfSearch(final String searchMessage){
        this.findById(fieldSearchIdLocator).sendKeys(searchMessage);
        this.findById(buttonSearchIdLocator).click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlContains("search"));
        List<WebElement> list = driver.findElements(emailCssLocator);
        int size = list.size();
        for (int i=0; i<size; i++){
            if(!list.get(i).isDisplayed()){
                list.remove(i);
                size--;
                i--;
            }
        }

        //Legassy
        for (int i=0; i<list.size(); i++){
            List<String> list1 = new ArrayList<>();
            list1.add(list.get(i).findElement(By.cssSelector("td.apU.xY")).getText());
            list1.add(list.get(i).findElement(By.cssSelector("td.xY.a4W")).getText());
            list1.add(list.get(i).findElement(By.cssSelector("td.xW.xY")).getText());
            listSearchResults.add(list1);
        }

        return list.size();
    }

    public AccountPage writeEmail(final String email, final String topic, final String message){
        this.findByClassName(buttonWriteClassNameLocator).click();
        this.waitVisibilityOfElement(this.findByCss(emailInputCssLocator));
        this.findByCss(emailInputCssLocator).sendKeys(email);
        this.findByCss(topicInputCssLocator).sendKeys(topic);
        WebElement messageElement =  this.findByCss(bodyMailCssLocator);
        new Actions(driver).moveToElement(messageElement).click().sendKeys(message).perform();
        this.findByCss(buttonSendMailCssLocator).click();
        return this;
    }

    public LoginPage logOut(){
        this.findByCss(logOutIconCssLocator).click();
        this.findById(logOutButtonIdLocator).click();
        return new  LoginPage(driver);
    }


    public List<List<String>> getListSearchResults() {
        return listSearchResults;
    }

//======================details========================================================

    enum Directories{
        inbox, sent, spam;
    }

    private List<List<String>> listSearchResults = new ArrayList<>();

    private boolean isNeedleDirectory(final String needle){
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlContains(needle.toLowerCase()));
        String url = this.driver.getCurrentUrl();
        return needle.toLowerCase().equals(url.toLowerCase().substring(url.length()-needle.length()));
    }

}
