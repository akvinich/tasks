package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public abstract class AbstractPage {

    protected WebDriver driver;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
    }

    protected void open(String relativePath) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(relativePath);
    }

    protected WebElement findByXPath(String xpathLocator) {
        return (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated((By.xpath(xpathLocator))));
    }

    protected WebElement findById(String id) {
        return (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated((By.id(id))));
    }

    protected WebElement findByCss(String cssLocator) {
        return (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated((By.cssSelector(cssLocator))));
    }

    protected WebElement findByClassName(String className) {
        return (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated((By.className(className))));
    }

    protected void clickLink(String linkText) {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated((By.linkText(linkText)))).click();
    }

    protected void clickPartLink(String linkText) {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated((By.partialLinkText(linkText)))).click();
    }

    protected void clickWithJavascriptExecutor(WebElement element){
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
    }

    protected void sendKeysWithJavascriptExecutor(final String keys, WebElement element){
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].value='"+ keys +"';", element);
    }

    protected void scrollIntoViewWithJavascriptExecutor(final String keys, WebElement element){
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("var select = arguments[0]; for(var i = 0; i < select.options.length; i++){ if(select.options[i].text == arguments[1]){ select.options[i].selected = true; } }", element, keys);
    }

    protected void waitVisibilityOfElement(WebElement element){
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
    }


}
