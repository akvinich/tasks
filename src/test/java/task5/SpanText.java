package task5;

public class SpanText {

    private String searchExpression;
    private String text;

    public SpanText(String searchExpression, String text) {
        this.searchExpression = searchExpression;
        this.text = text;
    }

    @Override
    public String toString() {
        return "searchExpression='" + searchExpression + '\'' +
                ", text='" + text + '\'';
    }
}
