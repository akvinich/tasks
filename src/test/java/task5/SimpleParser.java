package task5;

import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomText;
import com.gargoylesoftware.htmlunit.html.HtmlPage;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleParser {

    public SimpleParser(WebClient webClient) {
        this.webClient = webClient;
        this.webClient.getOptions().setJavaScriptEnabled(true);
        // this.webClient.getOptions().setCssEnabled(false);
        this.webClient.getOptions().setUseInsecureSSL(true);
        this.webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        this.webClient.getCookieManager().setCookiesEnabled(true);
        this.webClient.setAjaxController(new NicelyResynchronizingAjaxController());
        this.webClient.getOptions().setThrowExceptionOnScriptError(false);
        this.webClient.getCookieManager().setCookiesEnabled(true);
    }

    public HtmlPage openWebResource(final String url) throws IOException {
        HtmlPage page = this.webClient.getPage(url);
        webClient.waitForBackgroundJavaScript(30 * 1000);
        return page;
    }

    public List<String> getListTextFromElementByXPath(final String xPathExpression, HtmlPage page){
        List<DomText> listDom = page.getByXPath(xPathExpression+"/text()");
        List<String> list = new ArrayList<>();
        for (int i=0; i<listDom.size(); i++){
            list.add(listDom.get(i).asText());
        }
        return list;
    }

    public List<SpanText> findAllMentioning(final String expression, List<String> list){
        List<SpanText> spanTextList = new ArrayList<>();
        for (int i=0; i<list.size(); i++){
            if (containsIgnoreCase(list.get(i).toString(), expression)){
                spanTextList.add(new SpanText(expression, list.get(i)));
            }
        }
        return spanTextList;
    }


    private WebClient webClient = null;

    //=============================details====================================================

    private boolean containsIgnoreCase(final String haystack, final String needle){
        if(needle.equals(""))
            return true;
        if(haystack == null || needle == null || haystack .equals(""))
            return false;

        Pattern p = Pattern.compile(needle,Pattern.CASE_INSENSITIVE+Pattern.LITERAL);
        Matcher m = p.matcher(haystack);
        return m.find();
    }


}

