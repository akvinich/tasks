package task5;


import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

public class ParserTest {

    final static Logger rootLogger = LogManager.getRootLogger();

    @Test
    public void parseTest() throws IOException {

        SimpleParser simpleParser = new SimpleParser(new WebClient());

        HtmlPage htmlPage = simpleParser.openWebResource("https://edition.cnn.com");

        List<String> list =
                simpleParser.getListTextFromElementByXPath("//span[@class='cd__headline-text']", htmlPage);

        rootLogger.info(simpleParser.findAllMentioning("Trump", list));


    }

}
