package base;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import selenium.WebDriverFactory;

import java.util.concurrent.TimeUnit;

public class BaseChromeTest {
    protected WebDriver chrome;

    @BeforeTest
    public void configureBrowser(){
        chrome = WebDriverFactory.configureChrome();
        chrome.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterTest
    public void closeBrowser(){
        chrome.quit();
    }

}
