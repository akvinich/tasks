package task1.elements;

import elements.AbstractElement;
import org.openqa.selenium.*;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ListOfItems extends AbstractElement {

    By resultLocator = By.cssSelector("li.b-results__li");
    By linkLocator = By.cssSelector("a[target=_blank]");

    public ListOfItems(WebElement element) {
        super(element);
    }

    public int getQuantityOfResults(){
        return element.findElements(resultLocator).size();
    }

    public void clickFirstResultWithText(final String text){
        List<WebElement> listOfElements = element.findElements(resultLocator);
        int size = listOfElements.size();
        for (int i=0; i<size; i++){
            WebElement el = listOfElements.get(i);
            if (containsIgnoreCase(el.getText(), text)) {
                el.findElement(linkLocator).click();
                break;
            }
            if(i==size-1){
                throw new WebDriverException();
            }
        }
    }


    //=======================details==================================

    private boolean containsIgnoreCase(final String haystack, final String needle){
        if(needle.equals(""))
            return true;
        if(haystack == null || needle == null || haystack .equals(""))
            return false;

        Pattern p = Pattern.compile(needle,Pattern.CASE_INSENSITIVE+Pattern.LITERAL);
        Matcher m = p.matcher(haystack);
        return m.find();
    }
}
