package task1.elements;


import elements.AbstractElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SearchPanel extends AbstractElement {

    By searchInputLocator = By.id("search_from_str");
    By findButton = By.name("search");

    public SearchPanel(WebElement element) {
        super(element);
    }

    public void find(final String text){
        element.findElement(searchInputLocator).sendKeys(text);
        element.findElement(findButton).click();
    }


}
