package task1.pages;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import pages.AbstractPage;
import task1.elements.SearchPanel;

import java.util.Iterator;
import java.util.Set;

public class TutByMainPage extends AbstractPage{

    private final String formIdLocator ="search";

    public TutByMainPage(WebDriver driver) {
        super(driver);
    }

    final static Logger rootLogger = LogManager.getRootLogger();

    public TutByMainPage open(){
        this.open("https://www.tut.by/");
        rootLogger.info("go to: "+"https://www.tut.by/");
        return new TutByMainPage(driver);
    }

    public TutBySearchPage findInformation(final String text){
        SearchPanel searchPanel = new SearchPanel(this.findById(formIdLocator));
        searchPanel.find(text);

        String currentWindowHandle = driver.getWindowHandle();
        Set<String> windowHandles = driver.getWindowHandles();
        Iterator<String> it = windowHandles.iterator();
        while (it.hasNext()){
            String idHandle = it.next();
            if (idHandle.equals(currentWindowHandle)){}
            else {
                driver.switchTo().window(idHandle);
            }
        }
        return new TutBySearchPage(driver);
    }



}
