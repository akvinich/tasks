package task1.pages;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.AbstractPage;
import task1.elements.ListOfItems;

public class TutBySearchPage extends AbstractPage{
    final static Logger rootLogger = LogManager.getRootLogger();
    private final String listOfItemsClassNameLocator ="col-2";
    private ListOfItems listOfItems;

    public TutBySearchPage(WebDriver driver) {
        super(driver);
        rootLogger.info("go to: TutBySearchPage");
        this.listOfItems = new ListOfItems(findByClassName(listOfItemsClassNameLocator));
    }

    public int getCountOfItem(){
        return listOfItems.getQuantityOfResults();
    }

    public void followTheLink(final String text){
        listOfItems.clickFirstResultWithText(text);
    }


}
