package task1.tests;

import base.BaseFirefoxTest;
import org.openqa.selenium.WebDriverException;
import org.testng.annotations.*;
import task1.pages.TutByMainPage;
import task1.pages.TutBySearchPage;

public class FirstFirefoxTest extends BaseFirefoxTest{

    @DataProvider
    public Object[][] task1(){
        return new Object[][]{
                {new String("automated testing"), new String("Minsk Automated Testing Community")},
        };
    }

    @Test(dataProvider = "task1", expectedExceptions = WebDriverException.class)
    public void searchTest(String inputSearch, String findSearch){
        TutBySearchPage tutBySearchPage = new TutByMainPage(firefox).open().findInformation(inputSearch);
        int size = tutBySearchPage.getCountOfItem();
        tutBySearchPage.followTheLink(findSearch);
    }


}
